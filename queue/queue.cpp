#include "stdafx.h"
#include <iostream>
#include <queue>
#include <Windows.h>    /* Sleep */
#include <process.h>    /* _beginthread, _endthread */

using std::cout;
using std::endl;
using std::queue;
typedef queue<int> intQueue;

struct ThreadData
{
    CRITICAL_SECTION cs;
    intQueue q;
};

static void produce(void* pData)
{
    ThreadData* pTd = reinterpret_cast<ThreadData*>(pData);
    cout << "Producer thread started.\n";
    for (int i=0; i<10; i++)
    {
        printf("producing %d\n", i);
        EnterCriticalSection(&pTd->cs);
        pTd->q.push(i);
        LeaveCriticalSection(&pTd->cs);
    }
    cout << "Producer thread finished.\n";
    _endthread();
}

static void consume(void* pData)
{
    ThreadData* pTd = reinterpret_cast<ThreadData*>(pData);
    cout << "Consumer thread started.\n";
    int i=0;
    do
    {
        bool consumed = false;
        EnterCriticalSection(&pTd->cs);
        if (!pTd->q.empty())
        {
            i = pTd->q.front();
            pTd->q.pop();
            consumed = true;
        }
        LeaveCriticalSection(&pTd->cs);
        if (consumed)
        {
            printf("consumed  %d\n", i);
        }
    }
    while (i<9);
    cout << "Consumer thread finished.\n";
    _endthread();
}


int _tmain(int argc, _TCHAR* argv[])
{
    static ThreadData td;
    InitializeCriticalSection(&td.cs);
    cout << "Synchronized queue example" << endl;
    _beginthread(produce, 0, reinterpret_cast<void*>(&td));
    _beginthread(consume, 0, reinterpret_cast<void*>(&td));
    Sleep(1000);
    cout << "Press any key to finish." << endl;
    getchar();
    DeleteCriticalSection(&td.cs);
    return EXIT_SUCCESS;
}
